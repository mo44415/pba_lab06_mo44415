package pba.lab06.mapper;

import org.springframework.stereotype.Component;
import pba.lab06.entity.UserEntity;
import pba.lab06.model.User;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {
    public User mapToUser(UserEntity userEntity) {
        User user = new User();

        user.setId(userEntity.getId());
        user.setAge(userEntity.getAge());
        user.setCitizenship(userEntity.getCitizenship());
        user.setName(userEntity.getName());
        user.setEmail(userEntity.getEmail());
        user.setPersonalId(userEntity.getPersonalId());
        user.setSurname(userEntity.getSurname());

        return user;
    }

    public List<User> mapToUserList(List<UserEntity> userEntities) {
        return userEntities.stream().map(this::mapToUser).collect(Collectors.toList());
    }

    public UserEntity mapToUserEntity(User user) {
        UserEntity userEntity = new UserEntity();

        userEntity.setAge(user.getAge());
        userEntity.setCitizenship(user.getCitizenship());
        userEntity.setName(user.getName());
        userEntity.setEmail(user.getEmail());
        userEntity.setPersonalId(user.getPersonalId());
        userEntity.setSurname(user.getSurname());

        return userEntity;
    }
}
