package pba.lab06.auth.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pba.lab06.auth.utils.AuthUtils;
import pba.lab06.model.CreateRequest;

@Service
public class AuthService {
    @Autowired
    private AuthUtils authUtils;

    @Autowired
    private ObjectMapper objectMapper;

    public String generate(CreateRequest body) {
        try {
            return authUtils.generate(objectMapper.writeValueAsString(body));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
