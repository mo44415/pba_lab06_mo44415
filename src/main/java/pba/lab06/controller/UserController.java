package pba.lab06.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pba.lab06.api.UsersApi;
import pba.lab06.model.CreateRequest;
import pba.lab06.model.UpdateRequest;
import pba.lab06.model.UserListResponse;
import pba.lab06.model.UserResponse;
import pba.lab06.proxy.UserProxy;
import pba.lab06.service.UserService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(path = "/api")
@RequiredArgsConstructor
public class UserController implements UsersApi {

    private final UserService service;
    private final UserProxy proxy;

    @Override
    public ResponseEntity<UserResponse> createUser(@Valid CreateRequest body) {
        return ResponseEntity.ok().body(proxy.mapToUserResponse(service.save(proxy.mapToUserEntity(body.getUser()))));
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {
        return ResponseEntity.ok().body(proxy.mapToUserListResponse(service.findAll()));
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(UUID id) {
        return ResponseEntity.ok().body(proxy.mapToUserResponse(service.findById(id)));
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(UUID id, UpdateRequest body) {
        return ResponseEntity.ok().body(proxy.mapToUserResponse(service.update(id, proxy.mapToUserEntity(body.getUser()))));
    }
}
