package pba.lab06.auth.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import pba.lab06.auth.constants.AuthHeaders;
import pba.lab06.auth.exception.ErrorHandler;
import pba.lab06.auth.service.AuthService;
import pba.lab06.model.CreateRequest;
import pba.lab06.model.Error;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;


@Component
@Order(1)
public class HttpFilter extends OncePerRequestFilter {
    final static byte[] PASS_PHRASE = "123456".getBytes();

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AuthService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        CachedRequestHttpServletRequest cachedRequestHttpServletRequest = new CachedRequestHttpServletRequest(request);

        if (!cachedRequestHttpServletRequest.getRequestURL().toString().contains("/api/auth")) {
            if (cachedRequestHttpServletRequest.getMethod().equals("POST")) {
                HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, PASS_PHRASE);
                String req = IOUtils.toString(cachedRequestHttpServletRequest.getInputStream());
                final String signature = hmacUtils.hmacHex(req);

                if (!signature.equals(cachedRequestHttpServletRequest.getHeader(AuthHeaders.X_HMAC_SIGNATURE.getHeaderName()))) {
                    handleException(response);
                    return;
                }
            }
            if (cachedRequestHttpServletRequest.getMethod().equals("PUT")) {
                String req = IOUtils.toString(cachedRequestHttpServletRequest.getInputStream());
                String token = authService.generate(objectMapper.readValue(req, CreateRequest.class));

                if (!token.equals(cachedRequestHttpServletRequest.getHeader(AuthHeaders.X_JWS_SIGNATURE.getHeaderName()))) {
                    handleException(response);
                    return;
                }
            }
        }
        filterChain.doFilter(cachedRequestHttpServletRequest, response);
    }

    private void handleException(HttpServletResponse response) throws IOException {
        Error error = ErrorHandler.handle(HttpStatus.UNPROCESSABLE_ENTITY);
        response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        response.getWriter().write(objectMapper.writeValueAsString(error));
    }

    private static class CachedRequestHttpServletRequest extends HttpServletRequestWrapper {
        private final byte[] cachedRequestHttpServletRequest;

        public CachedRequestHttpServletRequest(HttpServletRequest request) throws IOException {
            super(request);
            this.cachedRequestHttpServletRequest = StreamUtils.copyToByteArray(request.getInputStream());
        }

        @Override
        public ServletInputStream getInputStream() {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(this.cachedRequestHttpServletRequest);

            return new ServletInputStream() {
                @Override
                public boolean isFinished() {
                    return inputStream.available() == 0;
                }

                @Override
                public boolean isReady() {
                    return true;
                }

                @Override
                public int read() {
                    return inputStream.read();
                }

                @Override
                public void setReadListener(ReadListener readListener) {
                    throw new UnsupportedOperationException();
                }
            };
        }
    }

}
