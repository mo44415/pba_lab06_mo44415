package pba.lab06.auth.constants;

public enum AuthHeaders {
    X_JWS_SIGNATURE("X-JWS-SIGNATURE"),
    X_HMAC_SIGNATURE("X-HMAC-SIGNATURE");

    private final String headerName;

    AuthHeaders(String headerName) {
        this.headerName = headerName;
    }

    public String getHeaderName() {
        return headerName;
    }
}
