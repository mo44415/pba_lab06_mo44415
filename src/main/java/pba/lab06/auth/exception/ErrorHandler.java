package pba.lab06.auth.exception;

import org.joda.time.DateTime;
import org.springframework.http.HttpStatus;
import pba.lab06.model.Error;
import pba.lab06.model.ResponseHeader;

import java.util.UUID;

public class ErrorHandler {
    public static Error handle(HttpStatus httpStatus) {
        ResponseHeader responseHeader = new ResponseHeader();
        responseHeader.setSendDate(DateTime.now());
        responseHeader.setRequestId(UUID.randomUUID());

        Error error = new Error();
        error.setCode(String.valueOf(httpStatus.value()));
        error.setMessage(httpStatus.getReasonPhrase());
        error.setResponseHeader(responseHeader);

        return error;
    }
}
