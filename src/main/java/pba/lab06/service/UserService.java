package pba.lab06.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pba.lab06.entity.UserEntity;
import pba.lab06.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {

    protected static final Log logger = LogFactory.getLog(UserService.class);

    private final UserRepository repository;

    public List<UserEntity> findAll() {
        logger.info(String.format("Sending all entities from %s class", UserService.class.getSimpleName()));
        return repository.findAll();
    }

    public UserEntity findById(UUID id) {
        Optional<UserEntity> entity = repository.findById(id);

        if (entity.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not found");

        logger.info(String.format("Sending entity with id %s", entity.get().getId()));
        return entity.get();
    }

    public UserEntity delete(UUID id) {
        Optional<UserEntity> entity = repository.findById(id);

        if (entity.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not found");

        repository.delete(entity.get());
        logger.info(String.format("Deleting entity with id %s", entity.get().getId()));
        return entity.get();
    }

    public UserEntity save(UserEntity entity) {
        logger.info(String.format("Saving entity with name %s", entity.getName()));
        return repository.save(entity);
    }

    public UserEntity update(UUID id, UserEntity updatedEntity) {
        Optional<UserEntity> entity = repository.findById(id);
        if (entity.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Not found");

        updatedEntity.setId(entity.get().getId());

        logger.info(String.format("Updating entity with id %s", entity.get().getId()));
        return repository.save(updatedEntity);
    }
}
